<?php
require 'databaseconfig.php';
$conn = db_get_connection(); 
if (isset($_GET["sort"])) {
  $sort = $_GET["sort"];    
} else {
  $sort = "DESC";
}                 
if (isset($_GET['pageno2'])) {
  $pageno2 = $_GET['pageno2'];
} else {
 $pageno2 = 1;
}
$catidvalue2 = $_GET["category"];               
$n = 2;
$offset = ($pageno2 - 1) * $n;
$total_pages = page_counter_catpost($n, $conn, $catidvalue2);                  
$data = related_post_displayer($conn, $offset, $n, $sort, $catidvalue2);
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Clean Blog - Start Bootstrap Theme</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="css/clean-blog.min.css" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand" href="index.php">Test Post</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="addblog.php">Add Blog</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="category.php">Categories</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Page Header -->
  <header class="masthead" style="background-image: url('img/home-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h1>Category Related Posts</h1>
            <!-- <span class="subheading">A Blog Theme by Start Bootstrap</span> -->
          </div>
        </div>
      </div>
    </div>
  </header>

  <!-- Main Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">

      <div class="clearfix">
              <?php 
              if (isset($_GET['pageno2'])) {
                $pno = $_GET['pageno2'];
              } else {
                $pno = 1;
              }
              $tid = $_GET["tag"]; 
              ?>
            <form>
              <input type="hidden" name="pageno2" value="<?php echo $pno;?>">
              <input type="hidden" name="tag" value="<?php echo $tid;?>">
              <select name = "sort" class="float-right ralign" onchange= "this.form.submit()" >
                <option value = "" disabled selected>--select sort--</option>
                <option value = "ASC">Sort by Ascending</option>
                <option value = "DESC">Sort by Descending</option>
              </select>
            </form>        
        </div>
        <br>
          <?php
          if (isset($data)) {                
            foreach ($data as $row) {
              $val = $row["bid"];
              $str = $row["content"];
              $cont =  content_trimmer($str);                       
              echo '
                    <div class="post-preview"> 
                      <a href="post.php?id='.$val.'">
                      <h2 class="post-title">'.$row["title"].'</h2>
                      <h3 class="post-subtitle">'.$cont.'</h3>
                      </a><p class="post-meta">Posted by
                      <a href="#">Start Bootstrap</a>
                      on '.$row["date"].'</p>    
                      ';
              $data2 = tag_displayer($val, $conn);
              echo "<p>Tags: ";
              if (isset($data2)) { 
                foreach ($data2 as $row2) {
                  $tagidval = $row2["tid"];
                  echo '<a href="relatedposts.php?tag=' . $tagidval . '">#' . $row2["tags"] . ' </a>';
                }
              }
              echo "</p>";
              $data3 = category_displayer($val, $conn);
              echo "<p>Categories: ";
              if (isset($data3)) { 
                foreach ($data3 as $row3) {
                  $catidval = $row3["cid"];
                  echo '<a href="categoryposts.php?category=' . $catidval . '">#' . $row3["categories"] . ' </a>';
                }
              }
              echo "</p>
                     </div>
                     <hr>";
            }
          } else {
           echo "0 results";
          }         
          $conn = NULL;
          ?>
  <hr>

  <!-- Pager -->
  <div class="clearfix">
           <ul class="pagination">  
            <li  class="<?php if($pageno2 == 1){ echo 'disabled'; } ?>">
              <a class="btn btn-primary float-right" href="<?php if($pageno2 == 1){ echo '#'; } else { echo "relatedposts.php?tag=$tagidvalue2&sort=$sort&pageno2=".($pageno2 - 1); } ?>">Prev  </a>
            </li>
            <li  class="<?php if($pageno2 == $total_pages){ echo 'disabled'; } ?>">
              <a class="btn btn-primary float-right ralign" href="<?php if($pageno2 == $total_pages){ echo '#'; } else { echo "relatedposts.php?tag=$tagidvalue2&sort=$sort&pageno2=".($pageno2 + 1); } ?>">  Next</a>
            </li>  
          </ul>
        </div>
      </div>
    </div>
  </div>

  <!-- Footer -->
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <ul class="list-inline text-center">
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-facebook-f fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-github fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
          </ul>
          <p class="copyright text-muted">Copyright &copy; Your Website 2019</p>
        </div>
      </div>
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/clean-blog.min.js"></script>

</body>

</html>

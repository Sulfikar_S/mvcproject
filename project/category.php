<?php
require 'databaseconfig.php';
if(isset($_GET["status"])) { 
	echo '<script language="javascript">alert("Category Updated Successfully")</script>';
}
if(isset($_GET["status2"])) { 
	echo '<script language="javascript">alert("Category Deleted Successfully")</script>';
}
$conn = db_get_connection(); 
if (isset($_POST['addcat'])) {
  if (isset($_POST['cat'])) {
    $category = $_POST['cat'];
  }
  category_add($conn, $category);  
}                  
$data = category_display($conn);
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Clean Blog - Start Bootstrap Theme</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="css/clean-blog.min.css" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand" href="index.php">Test Post</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="addblog.php">Add Blog</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="category.php">Categories</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Page Header -->
  <header class="masthead" style="background-image: url('img/home-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h1>Categories</h1>
            <span class="subheading">Content categories</span>
          </div>
        </div>
      </div>
    </div>
  </header>

  <!-- Main Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">

      
        <br>
          <?php
          if (isset($data)) {                
            foreach ($data as $row) {
              $val = $row["cid"];
              $str = $row["categories"];                       
              echo '
                    <div class="post-preview"> 
											<h3 class="category-name" >'.$val.' : '.$str.'</h3>
											<a class="ralign" href="editcat.php?eid=' . $val . '"?>Edit </a>
											<br> 
                      ';
              ?>
							<a class="ralign" href="delete.php?cid=<?php echo $val; ?>" onclick="return confirm('Are you sure want to delete the category?')">Delete</a>
							<?php
              echo "<br>
										</div>
                     <hr>";
            }
          } else {
           echo "0 results";
          }         
          $conn = NULL;
          ?>
					<div class="clearfix">
      <form name="categoryform" action="category.php"  method="POST">

        <div class="control-group">
            <div class="form-group floating-label-form-group controls">
                <label>Category</label>
                    <input type="text" class="form-control" placeholder="Category" name="cat" required data-validation-required-message="Add user defined category here...">
                    <p class="help-block text-danger"></p>
            </div>
        </div>
        <div id="success"></div>
          <div class="form-group">
            <right>
              <button type="submit" class="btn btn-primary" name="addcat" id="addcat">Add Category</button>
            </right>
          </div>
        </form>
				<?php
				if (isset($_POST['addcat'])) {
					echo "New Category Added Successfully";
				}
				?>             
        </div>
  <hr>

  <!-- Pager
  <div class="clearfix">
           <ul class="pagination">  
            <li  class="<?php if($pageno2 == 1){ echo 'disabled'; } ?>">
              <a class="btn btn-primary float-right" href="<?php if($pageno2 == 1){ echo '#'; } else { echo "relatedposts.php?tag=$tagidvalue2&sort=$sort&pageno2=".($pageno2 - 1); } ?>">Prev  </a>
            </li>
            <li  class="<?php if($pageno2 == $total_pages){ echo 'disabled'; } ?>">
              <a class="btn btn-primary float-right ralign" href="<?php if($pageno2 == $total_pages){ echo '#'; } else { echo "relatedposts.php?tag=$tagidvalue2&sort=$sort&pageno2=".($pageno2 + 1); } ?>">  Next</a>
            </li>  
          </ul>
        </div>
      </div>
    </div>
  </div> -->

  <!-- Footer -->
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <ul class="list-inline text-center">
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-facebook-f fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-github fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
          </ul>
          <p class="copyright text-muted">Copyright &copy; Your Website 2019</p>
        </div>
      </div>
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/clean-blog.min.js"></script>

</body>

</html>
